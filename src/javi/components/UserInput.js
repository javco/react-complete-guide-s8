import {useState} from 'react'

const UserInput = props => {

  const [enteredUserName, setEnteredName] = useState('')
  const [enteredAge, setEnteredAge] = useState('')
  const [invalidAge, setInvalidAge] = useState(false)

  const userNameChangeHandler = (event) => {
    setEnteredName(event.target.value)
  }
  const ageChangeHandler = (event) => {
    setEnteredAge(event.target.value)
  }

  const submitHandler = (event) => {
    event.preventDefault()

    if( parseInt(enteredAge) < 0 || isNaN(parseInt(enteredAge)) ){
      setInvalidAge(true)
    }
    else{
      const user = {
        'id': Math.random(),
        'name': enteredUserName,
        'age': enteredAge
      }

      props.onAddUser(user)

      setInvalidAge(false)
      setEnteredAge('')
      setEnteredName('')
    }
    return;
  }

  return(
    <div>
      <form onSubmit={submitHandler}>
        <div>
          <label className="form-label">Username</label>
          <input type="text" className="form-control" value={enteredUserName} onChange={userNameChangeHandler} />
        </div>
        <div>
          <label className="form-label">Age (Years)</label>
          <input type="text" className="form-control" value={enteredAge} onChange={ageChangeHandler} />
        </div>
        <div>
          <button type="submit">Add User</button>
        </div>
      </form>
      { invalidAge &&
      <div>
        <h4>Invalid Input!!</h4>
        <p>Please enter a validage (&gt; 0)</p>
      </div>
      }
    </div>
  )

}

export default UserInput