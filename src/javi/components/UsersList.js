import User from './User'
const UsersList = props => {

  if(props.users.length <= 0) {
    return <p>No users</p>
  }

  return(
    <ul>
      {
        props.users.map((user) => (
          <User
            key={user.id}
            name={user.name}
            age={user.age}
            />
        ))
      }
    </ul>
  )
}

export default UsersList