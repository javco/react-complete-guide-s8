import React, {useState} from 'react';
// import UserInput from './javi/components/UserInput'
// import UsersList from './javi/components/UsersList'

// function App() {

//   const [users, setUsers] = useState([])

//   const addUserHandler = (user) => {
//     console.log('hola')
//     console.log(user)
//     setUsers((prevUsers) => {
//       return[user, ...prevUsers]
//     })
//   }

//   return (
//     <div>
//       <UserInput
//         onAddUser={addUserHandler}
//         />
//       <UsersList
//         users={users}
//         />
//     </div>
//   );
// }

import AddUser from './max/components/Users/AddUser'
import UsersList from './max/components/Users/UsersList'

function App() {

  const [usersList, setUsersList] = useState([])

  const addUserHandler = (uName, uAge) => {
    setUsersList((prevUsersList) => {
      return[...prevUsersList, {id: Math.random().toString(), name: uName, age: uAge}]
    })
  }

  return (
    <div>
      <AddUser
        onAddUser={addUserHandler}
        />
      <UsersList
        users={usersList}
        />

    </div>
  );
}

export default App;
